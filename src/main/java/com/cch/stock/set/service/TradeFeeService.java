package com.cch.stock.set.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cch.platform.service.BaseService;
import com.cch.platform.util.StringUtil;
import com.cch.platform.web.WebUtil;
import com.cch.stock.set.bean.SdStock;
import com.cch.stock.set.bean.SdTradeFee;
import com.cch.stock.set.dao.StockDao;
import com.cch.stock.set.dao.TradeFeeDao;

@Service
public class TradeFeeService{
	
	@Autowired
	private TradeFeeDao feeDao;
	
	@Autowired
	private StockDao stockDao;

	
	public Map<String,Object> getFee(Map<String,Object> params){
		SdStock stock=stockDao.get((String)params.get("stockCode"));
		params.put("stockMarket",stock.getStockMarket());	
		params.put("userId", WebUtil.getUser().getUserId());
		params.put("tradeCode", params.get("billCatory"));
		SdTradeFee tradeFee=feeDao.get(params);
		if(tradeFee==null){
			return null;
		}
		
		Map<String,Object> re=new HashMap<String, Object>();
		double feeCommision=this.calculateFee(tradeFee.getFeeCommisionBase(), tradeFee.getFeeCommisionRate(),
				tradeFee.getFeeCommisionMin(), tradeFee.getFeeCommisionMax(), params);
		re.put("feeCommision", feeCommision);
		double feeTax=this.calculateFee(tradeFee.getFeeTaxBase(), tradeFee.getFeeTaxRate(),
				tradeFee.getFeeTaxMin(), tradeFee.getFeeTaxMax(), params);
		re.put("feeTax", feeTax);
		double feeOther=this.calculateFee(tradeFee.getFeeOtherBase(), tradeFee.getFeeOtherRate(),
				tradeFee.getFeeOtherMin(), tradeFee.getFeeOtherMax(), params);
		re.put("feeOther", feeOther);
		re.put("feeAll",feeCommision+feeTax+feeOther);
		double mny=-StringUtil.parseDouble((String)params.get("stockAllpric"))-(feeCommision+feeTax+feeOther);	
		re.put("billMoney", mny);
		return re;
	}
	
	public double calculateFee(String base,double rate,double min,double max,Map<String,Object> params){
		double baseNum=StringUtil.parseDouble((String)params.get(base));
		double fee=baseNum*rate/100;
		if(fee<0)fee=-fee;
		if(min!=0&&fee<min) fee=min;
		if(max!=0&&fee>max) fee=max;
		return fee;
	}
	
	public List<SdTradeFee> getAll() throws Exception{
		Map<String,Object> params=new HashMap<String,Object>();
		params.put("userId", WebUtil.getUser().getUserId());
		List<SdTradeFee> re=feeDao.find(params);
		List<String> keytype=new ArrayList<String>();
		for(SdTradeFee tradeFee:re){
			keytype.add(tradeFee.getTradeCode()+","+tradeFee.getStockMarket());
		}
		
		List<Map> alltype=feeDao.findNamedQuery("SdTradeFee.find.alltype", params);
		for(Map one:alltype){
			String key=one.get("tradeCode")+","+one.get("stockMarket");
			if(!keytype.contains(key)){
				SdTradeFee tradeFee=new SdTradeFee();
				tradeFee.setTradeCode(one.get("tradeCode").toString());
				tradeFee.setStockMarket(one.get("stockMarket").toString());
				re.add(tradeFee);
			}
		}
		return re;
	}

	public void saveOrUpdate(SdTradeFee entity) {
		entity.setUserId(WebUtil.getUser().getUserId());
		feeDao.saveOrUpdate(entity);
	}
	
}