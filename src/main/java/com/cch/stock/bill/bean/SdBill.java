package com.cch.stock.bill.bean;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * SdBill entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "sd_bill")
public class SdBill implements java.io.Serializable {

	// Fields

	private Integer billId;
	private Integer billSerialNumber;
	private Integer userId;
	private Timestamp billDate;
	private String billBigcatory;
	private String billCatory;
	private Double billMoney;
	private String stockCode;
	private Integer stockCnt;
	private Double stockPrice;
	private Double stockInterest;
	private Double stockAllpric;
	private Double feeCommision;
	private Double feeTax;
	private Double feeOther;
	private Double feeAll;
	private String billDesc;
	private Timestamp createTime;
	private Timestamp updateTime;

	// Constructors

	/** default constructor */
	public SdBill() {
	}

	/** full constructor */
	public SdBill(Integer userId, Timestamp billDate, String billBigcatory,
			String billCatory, Double billMoney, String stockCode,
			Integer stockCnt, Double stockPrice, Double stockInterest,
			Double stockAllpric, Double feeCommision, Double feeTax,
			Double feeOther, Double feeAll, String billDesc,
			Timestamp createTime, Timestamp updateTime) {
		this.userId = userId;
		this.billDate = billDate;
		this.billBigcatory = billBigcatory;
		this.billCatory = billCatory;
		this.billMoney = billMoney;
		this.stockCode = stockCode;
		this.stockCnt = stockCnt;
		this.stockPrice = stockPrice;
		this.stockInterest = stockInterest;
		this.stockAllpric = stockAllpric;
		this.feeCommision = feeCommision;
		this.feeTax = feeTax;
		this.feeOther = feeOther;
		this.feeAll = feeAll;
		this.billDesc = billDesc;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "increment")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "bill_id", unique = true, nullable = false)
	public Integer getBillId() {
		return this.billId;
	}

	public void setBillId(Integer billId) {
		this.billId = billId;
	}

	@Column(name = "user_ID")
	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	@Column(name = "bill_serialnumber")
	public Integer getBillSerialNumber() {
		return this.billSerialNumber;
	}

	public void setBillSerialNumber(Integer billSerialNumber) {
		this.billSerialNumber = billSerialNumber;
	}

	@Column(name = "BILL_DATE", length = 19)
	public Timestamp getBillDate() {
		return this.billDate;
	}

	public void setBillDate(Timestamp billDate) {
		this.billDate = billDate;
	}

	@Column(name = "bill_bigcatory", length = 10)
	public String getBillBigcatory() {
		return this.billBigcatory;
	}

	public void setBillBigcatory(String billBigcatory) {
		this.billBigcatory = billBigcatory;
	}

	@Column(name = "bill_catory", length = 10)
	public String getBillCatory() {
		return this.billCatory;
	}

	public void setBillCatory(String billCatory) {
		this.billCatory = billCatory;
	}

	@Column(name = "BILL_MONEY", precision = 16, scale = 4)
	public Double getBillMoney() {
		return this.billMoney;
	}

	public void setBillMoney(Double billMoney) {
		this.billMoney = billMoney;
	}

	@Column(name = "stock_code", length = 10)
	public String getStockCode() {
		return this.stockCode;
	}

	public void setStockCode(String stockCode) {
		this.stockCode = stockCode;
	}

	@Column(name = "stock_cnt")
	public Integer getStockCnt() {
		return this.stockCnt;
	}

	public void setStockCnt(Integer stockCnt) {
		this.stockCnt = stockCnt;
	}

	@Column(name = "stock_price", precision = 16, scale = 4)
	public Double getStockPrice() {
		return this.stockPrice;
	}

	public void setStockPrice(Double stockPrice) {
		this.stockPrice = stockPrice;
	}

	@Column(name = "stock_interest", precision = 16, scale = 4)
	public Double getStockInterest() {
		return this.stockInterest;
	}

	public void setStockInterest(Double stockInterest) {
		this.stockInterest = stockInterest;
	}

	@Column(name = "stock_allpric", precision = 16, scale = 4)
	public Double getStockAllpric() {
		return this.stockAllpric;
	}

	public void setStockAllpric(Double stockAllpric) {
		this.stockAllpric = stockAllpric;
	}

	@Column(name = "fee_commision", precision = 16, scale = 4)
	public Double getFeeCommision() {
		return this.feeCommision;
	}

	public void setFeeCommision(Double feeCommision) {
		this.feeCommision = feeCommision;
	}

	@Column(name = "fee_tax", precision = 16, scale = 4)
	public Double getFeeTax() {
		return this.feeTax;
	}

	public void setFeeTax(Double feeTax) {
		this.feeTax = feeTax;
	}

	@Column(name = "fee_other", precision = 16, scale = 4)
	public Double getFeeOther() {
		return this.feeOther;
	}

	public void setFeeOther(Double feeOther) {
		this.feeOther = feeOther;
	}

	@Column(name = "fee_all", precision = 16, scale = 4)
	public Double getFeeAll() {
		return this.feeAll;
	}

	public void setFeeAll(Double feeAll) {
		this.feeAll = feeAll;
	}

	@Column(name = "bill_desc", length = 500)
	public String getBillDesc() {
		return this.billDesc;
	}

	public void setBillDesc(String billDesc) {
		this.billDesc = billDesc;
	}

	@Column(name = "create_time", length = 19)
	public Timestamp getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}

	@Column(name = "update_time", length = 19)
	public Timestamp getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

}