/**
 *
 * @description 下拉框扩展函数，实现扩展属性和下拉框内容动态变化
 * @version 1.1
 * @author chchsheng
 * 
 * 1.0 初始版本
 *     硕正下拉框扩展函数
 *     
 * 1.1 新增setSelectServer、setSelectLocal，
 * 	   支持html的下拉框
 * 	   公开synchLoadloadFlexSet接口，支持自定义名称
 */


plat.FlexUtil = function() {
	
	/**
	 * 存储值集
	 */
	var _datas={};
	
	/**
	 * 
	 */
	function loadData(flexsetcodes,params,callback){
		if(!flexsetcodes){
			return;
		}
		if(!params){
			params={};
		}
		params.FLEXSET_CODES=flexsetcodes;
		$.post(plat.fullUrl("/core/FlexsetController/queryByCode.do"), params,
			function(data) {
				if((typeof callback)=='function'){
					callback();
				}
				$.extend(_datas,data);
			});
	};
	
	/**
	 * 同步加载
	 * @name 给值集取别名
	 */
	function synchLoad(flexsetcode,params){
		if(!flexsetcode){
			return;
		}
		if(!params){
			params={};
		}
		params.FLEXSET_CODES=flexsetcode;
		var a=$.ajax({
			   type: "POST",
			   url: plat.fullUrl("/core/FlexsetController/queryByCode.do"),
			   data: params,
			   dataType:"json",
			   async : false
			});
		data=eval("("+a.responseText+")");
		$.extend(_datas,data);
		return data.flexsetcode;
	}
	
	function getName(flexsetcode,code){
		return getValue(flexsetcode,code,"name");
	}
	
	/**
	 * 取值函数
	 */
	function getValue(flexsetcode,code,attribute){
		if(!flexsetcode){
			return null;
		}
		var re=_datas[flexsetcode];
		if(!re){   //不存在同步加载值集
			synchLoad(flexsetcode);
			re=_datas[flexsetcode];
		}
		if(code&&re){
			for(var i=0;i<re.length;i++){
				if(code==re[i].code) break;
			}
			if(i<re.length)re=re[i];
			else re=null;
		}
		if(attribute&&re){
			re=re[attribute];
		}
		return re;
	};
	
	/**
	 * 返回对外接口
	 */
	return {
		loadData : loadData,
		getValue : getValue,
		getName : getName,
		synchLoad : synchLoad
	};
}();